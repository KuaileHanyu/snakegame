// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable.h"
#include "SnakeBase.h"

// Add default functionality here for any IInteractable functions that are not pure virtual.

void IInteractable::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
		}
	}

}
