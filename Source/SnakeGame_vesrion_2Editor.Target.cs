// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class SnakeGame_vesrion_2EditorTarget : TargetRules
{
	public SnakeGame_vesrion_2EditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "SnakeGame_vesrion_2" } );
	}
}
